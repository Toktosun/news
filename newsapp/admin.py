from django.contrib import admin
from .models import SocialPost, BannerQuestion, MediaContent

admin.site.register(SocialPost)
admin.site.register(BannerQuestion)
admin.site.register(MediaContent)
from django.shortcuts import render
from .models import SocialPost, BannerQuestion, MediaContent


def post_image(request):
    social_img = SocialPost.objects.all().order_by('-id')
    banner = BannerQuestion.objects.all().order_by('-id')
    media_content = MediaContent.objects.all().order_by('-id')
    return render(request, 'index.html', {"social_img_list": social_img, "banner_list": banner, "media_content_list": media_content})


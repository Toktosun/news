# Generated by Django 2.2.10 on 2020-02-05 15:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PostImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='', verbose_name='Фото поста')),
                ('link', models.URLField(blank=True, null=True, verbose_name='Ссылка на пост')),
            ],
            options={
                'verbose_name': ' Twitter пост',
                'verbose_name_plural': 'Twitter посты',
            },
        ),
    ]

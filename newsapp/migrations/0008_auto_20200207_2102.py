# Generated by Django 2.2.10 on 2020-02-07 21:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsapp', '0007_bannerquestion'),
    ]

    operations = [
        migrations.CreateModel(
            name='MediaContent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pub_date', models.CharField(max_length=25, null=True, verbose_name='Дата публикации')),
                ('title', models.CharField(max_length=60, verbose_name='Заголовок статьи')),
                ('image', models.ImageField(upload_to='media-post', verbose_name='Фотография статьи')),
                ('content_text', models.TextField(max_length=500, verbose_name='Краткое содержание статьи')),
                ('title_link', models.CharField(max_length=45, verbose_name='Название ссылки')),
                ('link', models.URLField(blank=True, null=True, verbose_name='Ссылка на статью')),
            ],
            options={
                'verbose_name': 'Статья',
                'verbose_name_plural': 'Статьи из разных источников',
            },
        ),
        migrations.AlterField(
            model_name='bannerquestion',
            name='content_text',
            field=models.TextField(max_length=1000, verbose_name='Содержание'),
        ),
        migrations.AlterField(
            model_name='bannerquestion',
            name='name',
            field=models.CharField(max_length=35, null=True, verbose_name='Заголовок'),
        ),
    ]

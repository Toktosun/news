# Generated by Django 2.2.10 on 2020-02-07 23:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsapp', '0009_auto_20200207_2117'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoContent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pub_date', models.CharField(blank=True, max_length=35, verbose_name='Дата публикации')),
                ('title', models.CharField(max_length=90, verbose_name='Название Youtube видео')),
                ('link', models.URLField(verbose_name='Ссылка на видео')),
            ],
            options={
                'verbose_name': 'Youtube видео',
                'verbose_name_plural': 'Youtube видео',
            },
        ),
        migrations.AlterField(
            model_name='mediacontent',
            name='content_text',
            field=models.TextField(blank=True, max_length=500, verbose_name='Краткое содержание статьи'),
        ),
        migrations.AlterField(
            model_name='mediacontent',
            name='pub_date',
            field=models.CharField(blank=True, max_length=35, verbose_name='Дата публикации'),
        ),
    ]

# Generated by Django 2.2.10 on 2020-02-07 14:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsapp', '0005_auto_20200207_1235'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40, null=True, verbose_name='Название поста')),
                ('position', models.CharField(choices=[('right', 'Справа'), ('left', 'Слева')], max_length=100)),
                ('image', models.ImageField(upload_to='twitter/right side', verbose_name='Фото поста')),
                ('link', models.URLField(blank=True, null=True, verbose_name='Ссылка на пост')),
            ],
            options={
                'verbose_name': 'Пост',
                'verbose_name_plural': 'Посты',
            },
        ),
        migrations.DeleteModel(
            name='PostImageLeft',
        ),
        migrations.DeleteModel(
            name='PostImageRight',
        ),
    ]

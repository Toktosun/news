from django.db import models


POST_POSITIONS = (('right', 'Справа'), ('left', 'Слева'))


class SocialPost(models.Model):
    name = models.CharField(verbose_name="Название поста", max_length=40, null=True)
    position = models.CharField(choices=POST_POSITIONS, max_length=100)
    image = models.ImageField(verbose_name="Фото поста", upload_to="twitter/right side", blank=False)
    link = models.URLField(verbose_name="Ссылка на пост", blank=True, null=True)

    class Meta:
        verbose_name = "Пост"
        verbose_name_plural = "Посты из социальных сетей"

    def __str__(self):
        return self.name


class BannerQuestion(models.Model):
    name = models.CharField(verbose_name="Заголовок", max_length=50, null=True)
    content_text = models.TextField(verbose_name="Содержание", max_length=1000)

    class Meta:
        verbose_name = "Заголовок вопроса"
        verbose_name_plural = "Заголовки вопросов"

    def __str__(self):
        return self.name


class MediaContent(models.Model):
    pub_date = models.CharField(verbose_name="Дата публикации", max_length=35, blank=True)
    title = models.CharField(verbose_name="Заголовок статьи", max_length=120)
    image = models.ImageField(verbose_name="Фотография статьи",upload_to="media-post", blank=False)
    content_text = models.TextField(verbose_name="Краткое содержание статьи", max_length=500, blank=True)
    title_link = models.CharField(verbose_name="Название ссылки", max_length=50, blank=False)
    link = models.URLField(verbose_name="Ссылка на статью", blank=True, null=True)

    class Meta:
        verbose_name = "Статья"
        verbose_name_plural = "Статьи из разных источников"

    def __str__(self):
        return self.title

